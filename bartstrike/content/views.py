from django.http import HttpResponse,HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from models import BartEmployee, BartCSV

def index(request):
	my_csv_list = BartCSV.import_data(data = open("newest_salaries.csv"))
	context = {}
	context['employees'] = BartEmployee.objects.all()
	return render_to_response("index.html", context, context_instance=RequestContext(request))

