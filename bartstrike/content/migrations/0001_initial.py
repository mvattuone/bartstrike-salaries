# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'BartEmployee'
        db.create_table(u'content_bartemployee', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('entity', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('subtitle', self.gf('django.db.models.fields.CharField')(max_length=300, null=True, blank=True)),
            ('base', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('ot', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('other', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('mdv', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('er', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('ee', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('dc', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('misc', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('total_cost_of_employment', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'content', ['BartEmployee'])


    def backwards(self, orm):
        # Deleting model 'BartEmployee'
        db.delete_table(u'content_bartemployee')


    models = {
        u'content.bartemployee': {
            'Meta': {'object_name': 'BartEmployee'},
            'base': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'dc': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'ee': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'entity': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'er': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'mdv': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'misc': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'ot': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'other': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'subtitle': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '300', 'null': 'True', 'blank': 'True'}),
            'total_cost_of_employment': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['content']