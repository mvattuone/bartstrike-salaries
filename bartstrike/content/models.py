from django.db import models
from csvImporter.model import CsvDbModel

class BartEmployee(models.Model):
    entity = models.CharField(max_length=200,null=True,blank=True)
    name = models.CharField(max_length=300,null=True,blank=True)
    title = models.CharField(max_length=300,null=True,blank=True)
    subtitle = models.CharField(max_length=300,null=True,blank=True)
    base =  models.IntegerField(blank=True,null=True,help_text="Base pay for calendar year 2012")
    ot =  models.IntegerField(blank=True,null=True, help_text="Overtime pay in calendar year 2012")
    other = models.IntegerField(blank=True,null=True, help_text="Lump sump payouts for vacation, sick leave and comp time, bonuses and other taxable cash payments in 2011.")
    mdv = models.IntegerField(blank=True,null=True, help_text="Employer contributions to medical, dental, and vision plans.")
    er = models.IntegerField(blank=True,null=True, help_text="Employer contribution to pension")
    ee =  models.IntegerField(blank=True,null=True, help_text="Employee contribution to pension plan paid for by employer")
    dc =  models.IntegerField(blank=True,null=True, help_text="Employer contribution to deferred compensation (eg. 401(k) or 403(b) plans.")
    misc =  models.IntegerField(blank=True,null=True, help_text="Other non-cash costs of employment")
    total_cost_of_employment = models.IntegerField(blank=True,null=True)

    def __unicode__(self):
        return self.name,self.title,self.total_cost_of_employment

class BartCSV(CsvDbModel):

    class Meta:
           delimiter = ","
           dbModel = BartEmployee